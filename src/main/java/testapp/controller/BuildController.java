package testapp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.yaml.snakeyaml.Yaml;
import testapp.Build;

import java.io.*;
import java.util.Map;

@ResponseStatus(HttpStatus.OK)
@RestController
public class BuildController {

    @RequestMapping("/build")
    @PostMapping
    public String build(@RequestBody Build build) throws IOException, InterruptedException {

/*
        Yaml yaml = new Yaml();

        File dockerCompose = new File("../../app/config/docker-compose.yml");

        Map<String, Object> source = yaml.load(new FileInputStream(dockerCompose));

        Object services = source.get("services");

        Map<String, Object> servicesYaml = yaml.load(yaml.dump(services));
        Object kafka1Config = servicesYaml.get(build.getService());

        Map<String, Object> imageYml = yaml.load(yaml.dump(kafka1Config));
        Object image = imageYml.get("image");

        String test = yaml.dump(source);

        String test2 = test.replace(image.toString(), build.getBranch());

        FileWriter fw = new FileWriter(dockerCompose,false);
        fw.write(test2);
        fw.close();

        */



        ProcessBuilder pb = new ProcessBuilder("ssh", "-oStrictHostKeyChecking=no", "manuel@172.17.0.1", "./test.sh " + build.getBranch() + " " + build.getService());

        Process p = pb.start();

        System.out.println("SSH connection ok");

        PrintStream out = new PrintStream(p.getOutputStream());
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));

        //Process p = Runtime.getRuntime().exec("ssh -v -oStrictHostKeyChecking=no manuel@172.17.0.1");



        //out.println("./test.sh " + build.getBranch() + " " + build.getService());
        while (in.ready()) {
            String s = in.readLine();
            System.out.println(s);
        }

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        String Error;
        while ((Error = stdError.readLine()) != null) {
            System.out.println(Error);
        }
        while ((Error = stdInput.readLine()) != null) {
            System.out.println(Error);
        }

        out.println("exit");

        p.waitFor();
        System.out.println("SSH exit connection");

        return "ok";
    }
}
