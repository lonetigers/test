function send(event){
    var service = document.getElementById('exampleFormControlSelect1')
    var branchName = document.getElementById('exampleInputText').value;

    var serviceName = service[service.selectedIndex].text;

    var data = {
        service:serviceName,
        branch:branchName
    };

    var promise = new Promise(function(resolve, reject) {
        // do a thing, possibly async, then…
        var request = new XMLHttpRequest();

        request.open('POST', 'http://localhost:8081/build');
        request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        request.onload = function (ev) {
            if (request.status === 200) {
                resolve(request.response);
            }
            else {
                reject(Error("It broke"));
            }
        };
        request.send(JSON.stringify(data));

    });

    promise.then(function(){console.log("good")}, function(){console.log("bad")});

}


document.getElementById('build').addEventListener("click", send);